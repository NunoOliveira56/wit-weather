import 'dart:convert';

import 'package:http/http.dart';
import 'package:wit_weather/constants.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/services/internet_connection_service.dart';
import 'package:wit_weather/services/local_storage_service.dart';
import 'package:wit_weather/services/localization_service.dart';

abstract class WeatherRepository {
  Future<Weather> getWeatherByCityName(String cityName);

  Future<Weather> getWeatherByLocation(Location location);
}

class OpenWeatherMapRepository extends WeatherRepository {
  final SharedPreferencesLocalStorageService _localStorageService;
  final ConnectivityInternetConnectionService _connectionService;

  OpenWeatherMapRepository(this._localStorageService, this._connectionService);

  @override
  Future<Weather> getWeatherByCityName(String cityName) async {
    if (await _connectionService.isConnected()) {
      String url =
          'https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=${Constants.API_KEY}&units=metric';

      Response response = await get(url);

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      Map<String, dynamic> responseBody = json.decode(response.body);

      String iconCode = responseBody['weather'][0]['icon'];
      String imageSrc = 'https://openweathermap.org/img/wn/$iconCode@2x.png';

      responseBody['icon'] = imageSrc;

      Weather weather = Weather.fromAPI(responseBody);

      _localStorageService.saveWeather(weather);

      return weather;
    } else {
      return _localStorageService.getWeather(cityName);
    }
  }

  @override
  Future<Weather> getWeatherByLocation(Location location) async {
    if (await _connectionService.isConnected()) {
      String url =
          'https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&appid=${Constants.API_KEY}&units=metric';

      Response response = await get(url);

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      Map<String, dynamic> responseBody = json.decode(response.body);

      String iconCode = responseBody['weather'][0]['icon'];
      String imageSrc = 'https://openweathermap.org/img/wn/$iconCode@2x.png';

      responseBody['icon'] = imageSrc;

      Weather weather = Weather.fromAPI(responseBody, isLocal: true);

      _localStorageService.saveWeather(weather);

      return weather;
    } else {
      return null;
    }
  }
}
