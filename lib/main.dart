import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wit_weather/cubit/home/home_cubit.dart';
import 'package:wit_weather/repositories/weather_repository.dart';
import 'package:wit_weather/services/internet_connection_service.dart';
import 'package:wit_weather/services/local_storage_service.dart';
import 'package:wit_weather/services/localization_service.dart';

import 'routes.dart';

void main() {
  Routes.createRoutes();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeCubit>(
          create: (BuildContext context) => HomeCubit(
            OpenWeatherMapRepository(
              SharedPreferencesLocalStorageService(),
              ConnectivityInternetConnectionService(),
            ),
            LocalizationService(ConnectivityInternetConnectionService()),
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Wit Weather',
        theme: ThemeData(
          primarySwatch: customMaterialColor(Colors.white.value),
          cardColor: Colors.blue[200],
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        navigatorKey: Routes.sailor.navigatorKey,
        onGenerateRoute: Routes.sailor.generator(),
      ),
    );
  }

  MaterialColor customMaterialColor(int color) {
    Map<int, Color> colorCodes = {
      50: Color.fromRGBO(147, 205, 72, .1),
      100: Color.fromRGBO(147, 205, 72, .2),
      200: Color.fromRGBO(147, 205, 72, .3),
      300: Color.fromRGBO(147, 205, 72, .4),
      400: Color.fromRGBO(147, 205, 72, .5),
      500: Color.fromRGBO(147, 205, 72, .6),
      600: Color.fromRGBO(147, 205, 72, .7),
      700: Color.fromRGBO(147, 205, 72, .8),
      800: Color.fromRGBO(147, 205, 72, .9),
      900: Color.fromRGBO(147, 205, 72, 1),
    };

    return new MaterialColor(color, colorCodes);
  }
}
