import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wit_weather/services/internet_connection_service.dart';

class LocalizationService {
  final ConnectivityInternetConnectionService _connectionService;

  LocalizationService(this._connectionService);

  Future<Location> getCurrentLocation() async {
    Position position;

    await Permission.locationWhenInUse.request();

    if (await _connectionService.isConnected()) {
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
    } else {
      position = await Geolocator.getLastKnownPosition();
    }

    return Location(position.longitude, position.latitude);
  }
}

class Location {
  final double longitude;

  final double latitude;

  Location(this.longitude, this.latitude);
}
