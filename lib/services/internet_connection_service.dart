import 'package:connectivity/connectivity.dart';

class ConnectivityInternetConnectionService {
  ConnectivityInternetConnectionService();

  Future<bool> isConnected() async {
    return (await Connectivity().checkConnectivity()) !=
        ConnectivityResult.none;
  }
}
