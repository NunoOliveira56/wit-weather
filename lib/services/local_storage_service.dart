import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:wit_weather/entities/weather.dart';

abstract class LocalStorageService {
  Future<Weather> getWeather(String cityName);

  Future<void> saveWeather(Weather weather);
}

class SharedPreferencesLocalStorageService extends LocalStorageService {
  @override
  Future<Weather> getWeather(String cityName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (sharedPreferences.containsKey(cityName)) {
      return Weather.fromJson(
          json.decode(sharedPreferences.getString(cityName)));
    } else {
      return null;
    }
  }

  @override
  Future<void> saveWeather(Weather weather) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setString(
      weather.locationName,
      json.encode(
        weather.toJson(),
      ),
    );
  }
}
