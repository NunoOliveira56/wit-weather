part of 'home_cubit.dart';

@immutable
abstract class HomeState extends Equatable {}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => null;
}

class HomeLoading extends HomeState {
  @override
  List<Object> get props => null;
}

class HomeLoaded extends HomeState {
  final List<Weather> weathers;

  HomeLoaded(this.weathers);

  @override
  List<Object> get props => [weathers];
}
