import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/repositories/weather_repository.dart';
import 'package:wit_weather/services/localization_service.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final WeatherRepository _repository;
  final LocalizationService _localizationService;

  final List<String> cityNames = [
    'Lisbon',
    'Madrid',
    'Paris',
    'Berlin',
    'Bern',
    'Rome',
    'London',
    'Dublin',
    'Prague',
    'Vienna',
  ];

  HomeCubit(this._repository, this._localizationService) : super(HomeInitial());

  Future<void> getWeathers() async {
    final List<Weather> weathers = [];

    emit(HomeLoading());

    await Future.wait(
      cityNames.map<Future<void>>(
        (cityName) async {
          Weather weather = await _repository.getWeatherByCityName(cityName);
          if (weather != null) {
            weathers.add(weather);
          }
        },
      ).toList(),
    );

    weathers.sort((a, b) => a.locationName.compareTo(b.locationName));

    final Weather localWeather = await _repository.getWeatherByLocation(
      await _localizationService.getCurrentLocation(),
    );

    if (localWeather != null) {
      weathers.insert(0, localWeather);
    }

    emit(HomeLoaded(weathers));
  }
}
