import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Weather extends Equatable {
  final bool isLocal;

  final String locationName;

  final String imageSrc;

  final String weatherType;

  final double temperature;

  final double feelsLikeTemperature;

  final double minTemperature;

  final double maxTemperature;

  final int humidity;

  final double windSpeed;

  final int windDegrees;

  final int cloudiness;

  final double rainVolume;

  final double snowVolume;

  Weather({
    @required this.locationName,
    @required this.weatherType,
    @required this.imageSrc,
    @required this.temperature,
    @required this.feelsLikeTemperature,
    @required this.minTemperature,
    @required this.maxTemperature,
    @required this.humidity,
    @required this.windSpeed,
    @required this.windDegrees,
    @required this.cloudiness,
    this.rainVolume,
    this.snowVolume,
    this.isLocal = false,
  });

  factory Weather.fromAPI(Map<String, dynamic> json, {bool isLocal = false}) {
    return Weather(
      locationName: json['name'],
      imageSrc: json['icon'],
      weatherType: json['weather'][0]['main'],
      temperature: json['main']['temp'].toDouble(),
      feelsLikeTemperature: json['main']['feels_like'].toDouble(),
      minTemperature: json['main']['temp_min'].toDouble(),
      maxTemperature: json['main']['temp_max'].toDouble(),
      humidity: json['main']['humidity'],
      windSpeed: json['wind']['speed'].toDouble(),
      windDegrees: json['wind']['deg'],
      cloudiness: json['clouds']['all'],
      rainVolume: json.containsKey('rain') ? json['rain']['1h'] : null,
      snowVolume: json.containsKey('snow') ? json['snow']['1h'] : null,
      isLocal: isLocal,
    );
  }

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      locationName: json['locationName'],
      imageSrc: json['imageSrc'],
      weatherType: json['weatherType'],
      temperature: json['temperature'],
      feelsLikeTemperature: json['feelsLikeTemperature'],
      minTemperature: json['minTemperature'],
      maxTemperature: json['maxTemperature'],
      humidity: json['humidity'],
      windSpeed: json['windSpeed'],
      windDegrees: json['windDegrees'],
      cloudiness: json['cloudiness'],
      rainVolume: json['rainVolume'],
      snowVolume: json['snowVolume'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'locationName': locationName,
      'imageSrc': imageSrc,
      'weatherType': weatherType,
      'temperature': temperature,
      'feelsLikeTemperature': feelsLikeTemperature,
      'minTemperature': minTemperature,
      'maxTemperature': maxTemperature,
      'humidity': humidity,
      'windSpeed': windSpeed,
      'windDegrees': windDegrees,
      'cloudiness': cloudiness,
      'rainVolume': rainVolume,
      'snowVolume': snowVolume,
    };
  }

  @override
  List<Object> get props => [
        locationName,
        weatherType,
        imageSrc,
        temperature,
        feelsLikeTemperature,
        minTemperature,
        maxTemperature,
        humidity,
        windSpeed,
        windDegrees,
        cloudiness,
        rainVolume,
        snowVolume,
      ];
}
