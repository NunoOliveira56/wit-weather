// Routes class is created by you.
import 'package:sailor/sailor.dart';
import 'package:wit_weather/ui/home_page.dart';
import 'package:wit_weather/ui/weather_page/weather_page.dart';

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes(
      [
        SailorRoute(
          name: "/",
          builder: (context, args, params) {
            return HomePage();
          },
        ),
        SailorRoute(
          name: "/weather",
          builder: (context, args, params) {
            return WeatherPage(args: args);
          },
        ),
      ],
    );
  }
}
