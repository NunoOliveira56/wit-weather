import 'package:flutter/material.dart';

class InfoWidget extends StatelessWidget {
  final String label;
  final String text;

  const InfoWidget({
    Key key,
    @required this.text,
    @required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 4.0),
            child: Text(
              text,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Text(
            label,
            style: TextStyle(color: Colors.grey[600]),
          ),
        ],
      ),
    );
  }
}
