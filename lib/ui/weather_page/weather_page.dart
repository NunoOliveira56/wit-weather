import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/sections/rain_section_widget.dart';
import 'package:wit_weather/ui/weather_page/sections/snow_section_widget.dart';

import 'sections/cloudiness_section_widget.dart';
import 'sections/temperature_section_widget.dart';
import 'sections/wind_section_widget.dart';

class WeatherPageArgs extends BaseArguments {
  final Weather weather;
  final bool isCurrent;

  WeatherPageArgs(this.weather, {this.isCurrent = false});
}

class WeatherPage extends StatelessWidget {
  final Weather weather;
  final bool isCurrent;

  WeatherPage({Key key, @required WeatherPageArgs args})
      : weather = args.weather,
        isCurrent = args.isCurrent,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            isCurrent
                ? Padding(
                    padding: const EdgeInsets.only(right: 4.0),
                    child: Icon(Icons.place),
                  )
                : Container(),
            Text(weather.locationName),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                color: Theme.of(context).cardColor,
                height: MediaQuery.of(context).size.width / 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Center(
                        child: CachedNetworkImage(
                          imageUrl: weather.imageSrc,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 2,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              '${weather.temperature.toStringAsFixed(1)} ºC',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 40),
                            ),
                            Text(
                              'Current temperature',
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              children: [
                TemperatureSectionWidget(weather: weather),
                WindSectionWidget(weather: weather),
                CloudinessSectionWidget(weather: weather),
                RainSectionWidget(weather: weather),
                SnowSectionWidget(weather: weather),
              ],
            )
          ],
        ),
      ),
    );
  }
}
