import 'package:flutter/material.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_header_widget.dart';

class SectionWidget extends StatelessWidget {
  final String label;
  final IconData iconData;
  final List<InfoWidget> infoWidgets;

  const SectionWidget({
    Key key,
    @required this.label,
    @required this.iconData,
    @required this.infoWidgets,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionHeaderWidget(
          label: label,
          iconData: iconData,
        ),
        Divider(thickness: 1),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: infoWidgets,
          ),
        ),
        Divider(thickness: 1),
      ],
    );
  }
}
