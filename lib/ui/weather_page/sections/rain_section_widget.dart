import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_widget.dart';

class RainSectionWidget extends StatelessWidget {
  final Weather weather;

  const RainSectionWidget({
    Key key,
    @required this.weather,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (weather.rainVolume != null)
      return SectionWidget(
        label: 'Rain',
        iconData: MaterialCommunityIcons.weather_pouring,
        infoWidgets: [
          InfoWidget(
            label: 'Volume',
            text: '${weather.rainVolume.toString()} mm',
          ),
        ],
      );
    else
      return Container();
  }
}
