import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_widget.dart';

class SnowSectionWidget extends StatelessWidget {
  final Weather weather;

  const SnowSectionWidget({
    Key key,
    @required this.weather,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (weather.snowVolume != null)
      return SectionWidget(
        label: 'Snow',
        iconData: MaterialCommunityIcons.weather_snowy_heavy,
        infoWidgets: [
          InfoWidget(
            label: 'Volume',
            text: '${weather.snowVolume.toString()} mm',
          )
        ],
      );
    else
      return Container();
  }
}
