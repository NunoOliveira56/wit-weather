import 'package:flutter/material.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_widget.dart';

class TemperatureSectionWidget extends StatelessWidget {
  final Weather weather;

  const TemperatureSectionWidget({Key key, @required this.weather})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionWidget(
      label: 'Temperature',
      iconData: Icons.thermostat_rounded,
      infoWidgets: [
        InfoWidget(
          label: 'Max',
          text: '${weather.maxTemperature.toStringAsFixed(1)} ºC',
        ),
        InfoWidget(
          label: 'Min',
          text: '${weather.minTemperature.toStringAsFixed(1)} ºC',
        ),
        InfoWidget(
          label: 'Apparent',
          text: '${weather.feelsLikeTemperature.toStringAsFixed(1)} ºC',
        ),
      ],
    );
  }
}
