import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_widget.dart';

class WindSectionWidget extends StatelessWidget {
  final Weather weather;

  const WindSectionWidget({Key key, @required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionWidget(
      label: 'Wind',
      iconData: MaterialCommunityIcons.weather_windy,
      infoWidgets: [
        InfoWidget(
          label: 'Speed',
          text: '${weather.windSpeed.toString()} m/s',
        ),
        InfoWidget(
          label: 'Degrees',
          text: '${weather.windDegrees.toString()} º',
        ),
      ],
    );
  }
}
