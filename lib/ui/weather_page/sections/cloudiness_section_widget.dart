import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/ui/weather_page/widgets/info_widget.dart';

import 'section_widget.dart';

class CloudinessSectionWidget extends StatelessWidget {
  final Weather weather;

  const CloudinessSectionWidget({
    Key key,
    @required this.weather,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionWidget(
      label: 'Cloudiness',
      iconData: MaterialCommunityIcons.cloud,
      infoWidgets: [
        InfoWidget(
          label: 'Percentage',
          text: '${weather.cloudiness.toString()} %',
        ),
      ],
    );
  }
}
