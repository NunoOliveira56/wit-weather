import 'package:auto_animated/auto_animated.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wit_weather/cubit/home/home_cubit.dart';
import 'package:wit_weather/entities/weather.dart';
import 'package:wit_weather/routes.dart';
import 'package:wit_weather/ui/weather_page/weather_page.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();

    context.read<HomeCubit>().getWeathers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Wit Weather'),
      ),
      body: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) {
          if (state is HomeLoaded) {
            return RefreshIndicator(
              onRefresh: () => context.read<HomeCubit>().getWeathers(),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: SingleChildScrollView(
                  child: LiveList(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index,
                        Animation<double> animation) {
                      return buildAnimatedItem(
                        context,
                        index,
                        animation,
                        state.weathers.elementAt(index),
                      );
                    },
                    itemCount: state.weathers.length,
                  ),
                ),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  // Build animated item (helper for all examples)
  Widget buildAnimatedItem(
    BuildContext context,
    int index,
    Animation<double> animation,
    Weather weather,
  ) =>
      // For example wrap with fade transition
      FadeTransition(
        opacity: Tween<double>(
          begin: 0,
          end: 1,
        ).animate(animation),
        // And slide transition
        child: SlideTransition(
          position: Tween<Offset>(
            begin: Offset(0, -0.1),
            end: Offset.zero,
          ).animate(animation),
          // Paste you Widget
          child: WeatherWidget(weather: weather),
        ),
      );
}

class WeatherWidget extends StatelessWidget {
  final Weather weather;

  const WeatherWidget({
    Key key,
    @required this.weather,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Routes.sailor.navigate(
        '/weather',
        args: WeatherPageArgs(weather, isCurrent: weather.isLocal),
      ),
      child: Card(
        margin: EdgeInsets.fromLTRB(4, 4, 4, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CachedNetworkImage(
                      imageUrl: weather.imageSrc,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: AutoSizeText(
                        weather.weatherType,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        weather.locationName,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      weather.isLocal
                          ? Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Icon(Icons.place),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Center(
                  child: AutoSizeText.rich(
                    TextSpan(
                      text: '${weather.temperature.toStringAsFixed(1)}',
                      children: [
                        TextSpan(
                          text: 'ºC',
                          style: TextStyle(
                            color: Colors.grey[200],
                            fontSize: 25,
                          ),
                        ),
                      ],
                    ),
                    maxLines: 1,
                    style: TextStyle(fontSize: 50),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
