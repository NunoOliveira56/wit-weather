# Wit_Weather

## Description

This project is for a challenge from Wit Software to develop a simple weather app for the current location and 10 other cities.

This project has the following features implemented:
- Multiple weather information for 10 major cities ['Lisbon','Madrid','Paris','Berlin','Bern','Rome','London','Dublin','Prague','Vienna'];
- Multiple weather information for the current location (utilizes phone's location permission);
- Has offline storage for displaying the last recorded weather when last online;
- Simple and appealing UI for each city and current temperature, and a detailed view for access to the complete weather information of a single city.

Weather information was fetched using openweathermap.org/api.

All developed by Nuno Oliveira, and the entire history can be accessed in this repository.

## Executables

The debug and release executables are in "artifacts" section, next to the Clone button ("deploy-production" - release; "deploy-staging" - debug).

### Warning!

Currently there seems to be some issues with release APKs requesting permissions correctly, so I advise using the debug APK to test the app.
